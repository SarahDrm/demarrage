/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

import i18next from 'i18next'

import { formatNumber } from './util'
import moment from "moment"


export const setUiLocale = (locale) => {
    return fetch(`/translations/${locale}.json`)
            .then(response => response.json())
            .then(loadedResources => (
                new Promise((resolve, reject) => {
                    i18next
                    .init({
                        lng: locale,
                        debug: true,
                        resources: { [locale]: loadedResources },
                        interpolation: {
                            format: function (value, format, locale) {
                                if (value instanceof Date) {
                                   // return formatDate(value, format, locale)
                                    return moment(value).format(format);   
                                }
                                if ((format == "number" || format =="price") && value !== undefined ){
                                    return formatNumber(value, format, locale); 
                                }
                                return value
                            }
                        }
                    }, (err, t) => {
                        if (err) {
                            reject(err)
                            return
                        }

                        resolve()
                    })
                    
                })
            ))
            .catch(err => Promise.reject(err))
}

export const t = (key, opt) => i18next.t(key, opt)
