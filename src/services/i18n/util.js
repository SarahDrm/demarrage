/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

import { defaultLocale } from '../../config/i18n'
//import { prefixPath } from '../util'

import _ from 'lodash'

export function prefixPath (path, prefix) {
    return `/${prefix}/${_.trim(path, '/')}`
}

export function localizeRoutes (routes) {
    return routes.map(route => {
        // we default to localizing
        if (route.localize !== false)  { 
            return {
               ...route,
               path: prefixPath(route.path, ':locale')
            }
        }

        return { ...route }
    })
}

export function getLocaleFromPath(path) {
    if (path === "/") {
        return defaultLocale
    }
    return path.split('/')[1]
}

export function switchHtmlLocale (locale, dir) {
    const html =  window.document.documentElement

    html.lang = locale
    html.dir = dir

}

export function formatDate (value, format, locale) {
    // we transform "foo:bar;baz:man" into { foo: bar, baz: man }
    const options = {}
    format.split(';').forEach(part => {
        const [key, value] = part.split(':')
        options[key.trim()] = value.trim()
    })

    try {
        return new Intl.DateTimeFormat(locale, options).format(value)
    } catch (err) {
        console.error(err)
    }
}

export function formatNumber (value, format, locale) {
    
        switch(locale) {
        case "de": // 5.000.000,15
            return value.toLocaleString("de-DE");
            break;
        case "us": // 5,000,000.15
            return value.toLocaleString("en-US");
            break;
        default:   // 5 000 000,15
            return value.toLocaleString();
        }      
}
