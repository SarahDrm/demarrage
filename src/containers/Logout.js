/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Logout.css';
import { Link } from 'react-router-dom'


class Logout extends Component{

    render(){
        return(
            <div className= "Logout">                                               
                <Link to="/" > 
                    <img src={require("../logout.png")} 
                    class="imageLogout" />
                </Link>

            </div>
        );
    }
}

export default connect()(Logout);