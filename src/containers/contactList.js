/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday September 21st 2020
 */


import React, { useState } from 'react';
import MaterialTable from "material-table";

import { t } from '../services/i18n';
import PropTypes from 'prop-types';


ContactList.propTypes = {
  sourceContacts: PropTypes.any,
  callbackFromParent: PropTypes.any,
  dataListFromParent: PropTypes.array,
  sendDataToParentForEdit: PropTypes.any,
  sendDataToParentForAdd: PropTypes.any
};


export default function ContactList(props) {
  const { sourceContacts, callbackFromParent, dataListFromParent } = props;
    
    const [data, setData] = useState([]);
  
    const clientAccess = window.location.href.split('/')[window.location.href.split('/').length -1];
    {console.log("le clientAccess dans contactList est: " + clientAccess)}
  

    const handleDeleteRows = (event, rowData) => {
        props.handleDelete();
    };

    const handleAddRow = (event, rowData) => {
        props.handleAddRow();
      };  

    /*
    useEffect(async () => {
      //await fetch(`/api/contacts.json`)
      //await fetch(`/api/${this.props.sourceContacts}`)
      await fetch(`/api/${sourceContacts}`)
      .then(res => res.json())
      .then(items => {
        setData(items) // chargement des Sarah
        items.map(elt => {
          while (keyNames.length < 1){
            if (Object.keys(elt) !== "tableData" ){
              keyNames.push(Object.keys(elt))
            }
            keyNames[0].pop()
            keyNames[0].shift()
          }
        })
        for (let i=0; i < keyNames[0].length ; i++ ){
          const columnElement = new PaireColonnesContactList( keyNames[0][i]); 
          listColumns.push(columnElement);
        };
        setColumns(listColumns);
      })
    },[])*/

      
    // useEffect( 
    //   async() => {
    //     await setData(props.dataListFromParent);    
    //   //setData(props.dataListFromParent)
    //   }
    // ,[])
   
    return (
      <div className="ContactList">

        { 
          (clientAccess == "readOnly")?
         
            <MaterialTable
              title={t('list_contacts') + " readOnly"}
                columns = {[
                  { title: "name", field: "name" },
                  { title: "surname", field: "surname" },
                  { title: "phone", field: "phone" }
                  ]}
            
                data={props.dataListFromParent} // => solution de secours pour faire marcher transfertList
                options={{
                  cellStyle: {
                    lineHeight: 0.5
                  },
                  headerStyle: {
                    lineHeight: 0.5
                  }
                }}    
              />
            : 

            <MaterialTable
            title={t('list_contacts')+ " allRights"}
           // columns={columns}
              columns = {[
                { title: "name", field: "name" },
                { title: "surname", field: "surname" },
                { title: "phone", field: "phone" }
                ]}
          
              data={props.dataListFromParent} // => solution de secours pour faire marcher transfertList

              editable={{
                onRowAdd: newData =>
                new Promise((resolve, reject) => {
                  setTimeout(() => {
                    let results = [];
                    console.log(newData);
                    props.sendDataToParentForAdd(newData);
                    resolve()
                  }, 1000)
                })
                ,
                onBulkUpdate: (newData, oldData) =>
                new Promise((resolve, reject) => {
                  setTimeout(() => {
                    let results = [];
                    if(Object.values(newData)[0]!==undefined){
                      console.log("hello");
                      console.log(Object.values(newData)[0]);
                      console.log(Object.values(newData)[0]["newData"]);
                        Object.values(newData).forEach(element => {
                          results.push(element["newData"]);
                        }                
                        );
                    }

                    props.sendDataToParentForEdit(results) 
                    resolve();
                  }, 1000);
                })                
              //   , 
              //  onRowUpdate: (newData, oldData) =>  console.log("onRowUpdate")
           
              }}

              actions={[
                {
                  tooltip: 'Remove All Selected Users',
                  icon: 'delete', 
                  onClick: handleDeleteRows //pour la transfertList faire onClick: { rows => props.handleDeleteRows(rows)}
                  //onClick: { rows => props.handleDeleteRows(rows)}
                }
                /*,
              /*  {
                  tooltip: 'Edit',
                  icon: 'edit', 
                 // onClick: handleEditRows //pour la transfertList faire onClick: { rows => props.handleDeleteRows(rows)}
              /*    onClick: (newData, oldData) =>
                  new Promise((resolve, reject) => {
                    setTimeout(() => {
                    {
                      let _data = [...props.dataListFromParent];
                      const index = _data.indexOf(oldData);
                      _data[index] = newData;
                      setData(_data);
                    }
                    resolve()
                    }, 1000)
                  })		*/
         
                  //onClick: { rows => props.handleDeleteRows(rows)}
                  // onClick: (event, rowData) =>
                  // alert('You want to edit ' + rowData.name)
               // }
              ]}
              options={{
                selection: true,
                cellStyle: { lineHeight: 0.5 },
                headerStyle: { lineHeight: 0.5 }
              }}
              onSelectionChange = { rows => props.callbackFromParent(rows)}
            />
        }
      
      </div>
    );

}






