/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday September 8th 2020
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { t } from '../services/i18n';
import { useTranslation } from 'react-i18next';

import { withRouter } from 'react-router-dom'	
import { Address } from '../data/address.ts';
import './InternationalElements.css'


class InternationalElements extends Component{
      constructor(props) {
        super(props);
      }   

        render(){
        const currentDateTime = new Date();
        const number = 111222333.444;
        const price = 1000.55;
        const sarahAddress = new Address("Mlle", "Sarah", "Dormoy", "4", "Pasteur", "06000", "Nice", "France");
       

          return(
            <div className="InternationalElements">

              <p>
      
                {t('app_name')}  <br />
                - {t('welcome')} -  <br /> 
                - in  {t('country')} <br /> 
                date + Time: {t("Time", { time: currentDateTime })} <br />
                {t("Display current date", { date: new Date() })} 
                <br />
                {t("Display number format", { number: number , country: t('country') })} 
                <br />
                {t("Display price format", { price: price , country: t('country'), currency : t('currency') })} 
                <br />
                {t("Display Address format", { 
                    title: sarahAddress.title, 
                    name: sarahAddress.name,
                    surname: sarahAddress.surname,
                    number: sarahAddress.number,
                    street_name: sarahAddress.street_name,
                    city: sarahAddress.city,
                    postal_code: sarahAddress.postal_code,
                    country: sarahAddress.country,
                    street: t('street')
                    }) } 
                <br />

              </p>
            
            </div>
          );
      }    
}

export default withRouter(
  connect()(InternationalElements)
)


/*
export default function InternationalElements() {
  const { t } = useTranslation();

  const currentDateTime = new Date();
  const number = 111222333.444;
  const price = 1000.55;
  const sarahAddress = new Address("Mlle", "Sarah", "Dormoy", "4", "Pasteur", "06000", "Nice", "France");
  return (
    <div className="InternationalElements">
       <p>
      
        {t('app_name')}  <br />
        - {t('welcome')} -  <br /> 
        - in  {t('country')} <br /> 
        date + Time: {t("Time", { time: currentDateTime })} <br />
        {t("Display current date", { date: new Date() })} 
        <br />
        {t("Display number format", { number: number , country: t('country') })} 
        <br />
        {t("Display price format", { price: price , country: t('country'), currency : t('currency') })} 
        <br />
        {t("Display Address format", { 
            title: sarahAddress.title, 
            name: sarahAddress.name,
            surname: sarahAddress.surname,
            number: sarahAddress.number,
            street_name: sarahAddress.street_name,
            city: sarahAddress.city,
            postal_code: sarahAddress.postal_code,
            country: sarahAddress.country,
            street: t('street')
            }) } 
        <br />

    </p>
    </div>
  );
}*/