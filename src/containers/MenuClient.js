/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import './MenuClient.css';
import { getUsersFromJson } from '../actions/clientAction'
import { t } from '../services/i18n'
import { getLocaleFromPath, prefixPath } from '../services/i18n/util'
import { withRouter, Link, Redirect, useHistory } from 'react-router-dom'
import { Button, FormGroup, FormControl, FormLabel  } from "react-bootstrap";

class MenuClient extends Component{

      constructor(props) {
        super(props);
        this.state={
            clientAccess : "readOnly"
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }   

    componentDidMount(){
        this.props.getUsersFromJson(getLocaleFromPath(this.props.location.pathname)) //prend la langue et à partir de la va chercher la liste de Users   
    }

    handleChange(event) {
        this.setState({clientAccess: event.target.value});
    }

    handleSubmit(){
      this.props.history.push(prefixPath(`/VerticalTabs/${this.state.clientAccess}`, getLocaleFromPath(this.props.location.pathname)));
    }

    render(){
        
        return(
            <div className= "menuClient">
                <div className="Login">
                
                    <form onSubmit={this.handleSubmit}>
                    
                        <FormGroup controlId="email">
                            <FormLabel>Email</FormLabel>
                                <FormControl as="select" onChange={this.handleChange}>
                                {this.props.users.map(
                                    user => (       
                                    <option value={user.access_rights}>{t('user')}  {user.access_rights} </option>                            
                                        )                                
                                )}	
                                </FormControl>
                        </FormGroup>
                        <FormGroup controlId="password" bsSize="large">
                            <FormLabel >Password</FormLabel >
                            <FormControl type="password" />
                        </FormGroup>
                    
                        <Button block bsSize="large" type="submit">
                            Login
                        </Button>
                        
                    </form> 
                </div>

                <br />
                
            </div>
        );
    }    
} 
 

function mapStateToProps(state) {
    return {
        users: state.reduce.users,
        clientProfile: state.reduce.clientProfile
    };
  }

export default withRouter( // withRouter permet à la redirection de bien fonctionner
    connect(
        mapStateToProps,
        {getUsersFromJson}
    )(MenuClient)
)


