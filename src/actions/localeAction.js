/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */


export const changeLocale = (locale) => ({
    type: 'CHANGE_LOCALE',
    locale
})

export const setUiTranslationsLoading = () => ({
    type: 'SET_UI_TRANSLATIONS_LOADING'
})

export const setUiTranslationsLoaded = isLoaded => ({
    type: 'SET_UI_TRANSLATIONS_LOADED',
    isLoaded
})