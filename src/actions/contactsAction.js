/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Friday September 11th 2020
 */


import * as types from './actionTypes';


export const getContactsFromJson = (sourceContacts) => (dispatch) => {  
    return fetch(`/api/${sourceContacts}`)
                .then(response => response.json())
                .then(contacts => dispatch(getContacts(contacts)))
                .catch(err => console.error(err))
  }

/*  
export function getContactsFromJson(sourceContacts){
  console.log("contactAction " + sourceContacts)
  return (dispatch) => { 
    fetch(`/api/${sourceContacts}`)
              .then(response => response.json())
              .then(contacts => dispatch(getContacts(contacts)))
              .catch(err => console.error(err))
   }
} */

  
  export function getContacts(contacts) {
    return({ type: types.GET_CONTACTS, contacts });
  }