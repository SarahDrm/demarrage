
import * as types from './actionTypes';

//choix du profil utilisateur
/*
export function chooseClientProfileAction(clientAccess) {
  
    return({ type: types.CHOOSE_CLIENT_PROFILE, clientAccess });
  }*/


  export function chooseClientProfileAction(clientAccess) {
   return (dispatch) => { 
     dispatch(chooseClientProfile(clientAccess)) 
    }
  }

  const chooseClientProfile = clientAccess => ({
    type: types.CHOOSE_CLIENT_PROFILE,
    clientAccess
  })

  /*
export const getUsersFromJson = (locale) => (dispatch) => {  
  return fetch(`/api/${locale}/users.json`)
              .then(response => response.json())
              .then(users => dispatch(getUsers(users)))
              .catch(err => console.error(err))
}*/

export function getUsersFromJson(locale){
  return (dispatch) => { 
    fetch(`/api/${locale}/users.json`)
                .then(response => response.json())
                .then(users => dispatch(getUsers(users)))
                .catch(err => console.error(err))
   }
} 


export function getUsers(users) {
  return({ type: types.GET_USERS, users });
}


                