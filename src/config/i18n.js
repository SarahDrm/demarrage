/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

export const defaultLocale = "en"

export const locales = [
    {
        code: "en",
        name: "English"
    },
    {
        code: "fr",
        name: "Français"
    },
    {
        code: "es",
        name: "Español"
    },
    {
        code: "de",
        name: "Deutsch"
    },
    {
        code: "it",
        name: "Italiano"
    },
    {
        code: "us",
        name: "US English"
    }
]


