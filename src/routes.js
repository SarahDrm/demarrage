/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

import React from 'react'
import { Redirect } from 'react-router-dom'


import { defaultLocale } from './config/i18n'

import { localizeRoutes } from './services/i18n/util'

import InternationalElements from './containers/InternationalElements'
import MenuClient from './containers/MenuClient'
import VerticalTabs from './components/VerticalTabs'
import TransferList from './components/TransfertList'
import contactList from './containers/contactList'


const routes = [
    {
        path: "/",
        exact: true,
        localize: false,
        component: () => <Redirect to={`/${defaultLocale}/MenuClient`} />
    },
    {
        path: "/InternationalElements",
        component: InternationalElements
    },
    {
        path: "/MenuClient",
        component: MenuClient
    },
    {
        path: "/VerticalTabs",
        component: VerticalTabs
    },
    {
        path: "/TransfertList",
        component: TransferList
    },
    {
        path: "/ContactList",
        component: contactList
    }
]

export default localizeRoutes(routes)

