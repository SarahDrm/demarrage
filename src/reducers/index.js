
import * as client from './clientReducer';
import { reduce } from './clientReducer';
import { reduceI10n } from './I10n';
import { reduceContacts } from './contactsReducer';


  export default {
    client,
    reduce,
    reduceI10n,
    reduceContacts
    
  }
  