/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Friday September 11th 2020
 */


import * as types from '../actions/actionTypes';

const initialState = ({
   
    contacts :
    [
      {
          "id": 1,
          "name": "Reducer",
          "surname": "Reducer",
          "phone": "1111111111"
      },
      {
          "id": 2,
          "name": "Reducer",
          "surname": "Reducer",
          "phone": "2222222222"
      },
      {
          "id": 3,
          "name": "Reducer",
          "surname": "Reducer",
          "phone": "3333333333"
      }
  ]
  });
  
    export function reduceContacts(state = initialState , action  ) {
        
      switch(action.type){
        case types.GET_CONTACTS:  // si on choisit un profil user, alors on enregistre ce profil dans le state
        console.log("GET_CONTACTS")
        return {
            ...state,
            contacts: action.contacts
          }
          default: 
          console.log("DEFAULT")
            return state;
           /* return {
              ...state,
              contacts: action.contacts
            }*/
      }
  }