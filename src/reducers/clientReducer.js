import * as types from '../actions/actionTypes';

const initialState = ({
    clientProfile: "NONE", // à l'état initial le user n'est pas renseigné
    users: []
  });
  
    export function reduce(state = initialState, action  ) {
        
      switch(action.type){
        case types.CHOOSE_CLIENT_PROFILE:  // si on choisit un profil user, alors on enregistre ce profil dans le state
          console.log("reducer de choose client profile")  
          return {
                ...state,
                clientProfile: action.clientAccess
              }
        case types.GET_USERS:  // si on choisit un profil user, alors on enregistre ce profil dans le state
        return {
            ...state,
            users: action.users
          }
           
          default: 
            return state;
      }
  }




  
