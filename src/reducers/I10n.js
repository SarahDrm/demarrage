/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

import { defaultLocale } from '../config/i18n'

const INITIAL_STATE = {
    locale: defaultLocale,
    uiTranslationsLoaded: false, // si il a chargé ou pas le fichier de translation
}

export function reduceI10n(state = INITIAL_STATE, action  ) {

    switch(action.type) {
        case 'CHANGE_LOCALE': 
            return {
                ...state,
                locale: action.locale
            }

        case 'SET_UI_TRANSLATIONS_LOADING':
            return  {
                ...state,
                uiTranslationsLoaded: false,
            }
    
        case 'SET_UI_TRANSLATIONS_LOADED':
            return  {
                ...state,
                uiTranslationsLoaded: action.isLoaded,
            }
        default:
            return state
    }
}