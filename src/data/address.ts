/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 10th 2020
 */

export class Address {

    public readonly title: any;
    public readonly name: any;
    public readonly surname: any;
    public readonly number: any;
    public readonly street_name: any;
    public readonly postal_code: any;
    public readonly city: any;
    public readonly country: any;

    constructor(title: any, name: any, surname: any, number: any, street_name: any, postal_code: any, city: any, country: any) {
            
        this.title = title;
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.street_name = street_name;
        this.postal_code = postal_code;
        this.city = city;
        this.country = country;
    
      }
}