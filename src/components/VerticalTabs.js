/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday September 15th 2020
 */

import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import './VerticalTabs.css';
import { t } from '../services/i18n';
import TransferList from './TransfertList';
import ContactList from '../containers/contactList';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return { // essayer de faire un if -> si readonly return disable
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 524,
    marginTop: 100
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    width: 200
  },
}));

VerticalTabs.propTypes = {
  clientAccess: PropTypes.any.isRequired
};

export default function VerticalTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [list, setList] = React.useState([]); // elements du tableau
  const [checked, setChecked] = React.useState([]);

  const clientAccess = window.location.href.split('/')[window.location.href.split('/').length -1];
  
  useEffect(() => {
    fetchData('/api/contacts.json');
  }, []);

  async function fetchData(url){
    const response = await fetch(url);
    const json = await response.json();
    const dataArray = Array.from(json);
      setList(dataArray);
  };	

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleDelete = () => {
    let _data = [...list]; 
    checked.forEach(rd => {
      _data = _data.filter(t => t.tableData.id !== rd.tableData.id);
     });
    setList(_data); 
  };

  const handleAdd = (dataAddedInChild) => {
    let _data = [...list]; 
    _data.push(dataAddedInChild);
    setList(_data); 
  }; 

  const handleEdit = (dataModifiedInChild) => {
    let _data = [...list];
    dataModifiedInChild.forEach(element => {
      _data.forEach(eltdeData => {
        if (eltdeData["id"]==element["id"]){
          _data[_data.indexOf(eltdeData)] = element;
        }
      })
    });
    setList(_data);
  };
  
  const myCallback = (dataFromChild) => {
    if (dataFromChild !== undefined){
     setChecked(dataFromChild);   
    }
  };	 

  return (
    
    <div className={classes.root} >
     
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label={t('contacts')} {...a11yProps(0)} />

        {
            (clientAccess == "readOnly" || clientAccess == "read_write")?
            <Tab label={t('groups')} disabled /> 
            :
            <Tab label={t('groups')} {...a11yProps(1)} />
        }

      </Tabs>
      <TabPanel value={value} index={0}>
        <ContactList 
          dataListFromParent={list} 
          callbackFromParent={myCallback} 
          handleDelete={handleDelete} 
          sendDataToParentForEdit={handleEdit}
          sendDataToParentForAdd={handleAdd}
          
          />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <TransferList/>
      </TabPanel>
      
    </div>
  );
}
