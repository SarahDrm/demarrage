/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Wednesday September 2nd 2020
 */

import React, { Component }  from 'react';
import { connect } from 'react-redux';
import AppNavbar from '../components/AppNavbar';
import {  Route,
  Switch, BrowserRouter as Router } from 'react-router-dom';
import Localizer from '../containers/Localizer';
import routes from '../routes';

class App extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Router>

          <Localizer> 
          
            <AppNavbar /> 

              <div className="container">
                <main id="main" role="main">
                    <Switch>
                        {routes.map((route, index) => (
                            <Route
                                key={index}
                                path={route.path}
                                exact={route.exact}
                                component={route.component}
                            />
                        ))}
                    </Switch>
                </main>
              </div>

          </Localizer>
                                    
        </Router>

      </div>
    );
  }
}


function mapStateToProps(state) {
  return { 
    uiTranslationsLoaded: state.reduceI10n.uiTranslationsLoaded,
    clientProfile: state.reduce.clientProfile,
  };
}


export default connect(mapStateToProps)(App);

