/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Friday September 18th 2020
 */

import React, {useEffect, useCallback} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ContactList from '../containers/contactList';
import './TransfertList.css';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 200,
    height: 230,
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) { //a checked , b left
  return a.filter((value) => b.indexOf(value) !== -1);
}

export default function TransferList() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([]);

  const [trueListLeft, settrueListLeft] = React.useState([]);
  const [trueListRight, settrueListRight] = React.useState([]);

 
  useEffect(() => {
    fetchData('/api/contactsAmis.json');
    fetchData('/api/contactsFamille.json');
  }, []);

  async function fetchData(url){
    const response = await fetch(url);
    const json = await response.json();
    const dataArray = Array.from(json);
    if (url.includes('Amis')){
      settrueListLeft(dataArray);
    }else{
      settrueListRight(dataArray);
    }
  }
    
  const materialTableLeftChecked = intersection(checked, trueListLeft);
  const materialTableRightChecked = intersection(checked, trueListRight);

  const myCallback = (dataFromChild) => {
    if (dataFromChild !== undefined){
     setChecked(dataFromChild);   
    }
  };

  const handleCheckedRight = () => {
    settrueListRight(trueListRight.concat(materialTableLeftChecked));    // on prend l'element de gauche qu'on met dans la liste de droite
    settrueListLeft(not(trueListLeft, materialTableLeftChecked));        // on l'enlève de la liste de gauche
    setChecked(not(checked, materialTableLeftChecked));                  //décochage de la case
    checked.map((element) => {
      element.tableData.checked = false;
    })
  };


  const handleDelete = () => {

      let _data = [...trueListLeft]; // toutes les lignes
      materialTableLeftChecked.forEach(rd => {
        _data = _data.filter(t => t.tableData.id !== rd.tableData.id);
       });
      settrueListLeft(_data); // toutes les lignes - rowData
   
  };

  const handleDeleteRight = () => {
    let _data = [...trueListRight]; // toutes les lignes
    materialTableRightChecked.forEach(rd => {
      _data = _data.filter(t => t.tableData.id !== rd.tableData.id);
    });
    settrueListRight(_data);
  };


  const handleCheckedLeft = () => {
    settrueListLeft(trueListLeft.concat(materialTableRightChecked));
    settrueListRight(not(trueListRight, materialTableRightChecked));
    setChecked(not(checked, materialTableRightChecked));
    checked.map((element) => {
      element.tableData.checked = false;
    })
  };


  const handleEditLeft = (dataModifiedInChild) => {
    let _data = [...trueListLeft];
    dataModifiedInChild.forEach(element => {
      _data.forEach(eltdeData => {
        if (eltdeData["id"]==element["id"]){
          _data[_data.indexOf(eltdeData)] = element;
        }
      })
    });
    settrueListLeft(_data); //settrueListRight or settrueListLeft
  };

  const handleEditRight = (dataModifiedInChild) => {
    let _data = [...trueListRight];
    dataModifiedInChild.forEach(element => {
      _data.forEach(eltdeData => {
        if (eltdeData["id"]==element["id"]){
          _data[_data.indexOf(eltdeData)] = element;
        }
      })
    });
    settrueListRight(_data); //settrueListRight or settrueListLeft
  };

  const handleAddLeft = (dataAddedInChild) => {
		let _data = [...trueListLeft]; // toutes les lignes
		_data.push(dataAddedInChild);
		settrueListLeft(_data); // toutes les lignes - rowData
  }; 
  
  const handleAddRight = (dataAddedInChild) => {
		let _data = [...trueListRight]; // toutes les lignes
		_data.push(dataAddedInChild);
    settrueListRight(_data); // toutes les lignes - rowData
	}; 
 
  return (
    <div className="TransfertList">

    <br></br><br></br><br></br>
   
<Grid container spacing={2} justify="center" alignItems="center" className={classes.root}>
    <Grid item className="rightList">
      <ContactList 
        sourceContacts="contactsAmis.json" 
        callbackFromParent={myCallback}  
        dataListFromParent={trueListLeft} 
        handleDelete={handleDelete}
        sendDataToParentForEdit={handleEditLeft}
        sendDataToParentForAdd={handleAddLeft}

        /> 
    </Grid>  
    <Grid item>
      <Grid container direction="column" alignItems="center">
        <Button
          variant="outlined"
          size="small"
          className={classes.button}
          onClick={handleCheckedRight}
          disabled={materialTableLeftChecked.length === 0} // si liste de gauche est vide on disable
          aria-label="move selected right"
        >
          &gt;
        </Button>
        <Button
          variant="outlined"
          size="small"
          className={classes.button}
          onClick={handleCheckedLeft}
          disabled={materialTableRightChecked.length === 0} // si liste de droite est vide on disable
          aria-label="move selected left"
        >
          &lt;
        </Button>
      </Grid>
    </Grid>
    <Grid item className="leftList">
      <ContactList 
        sourceContacts="contactsFamille.json" 
        callbackFromParent={myCallback} 
        dataListFromParent={trueListRight}
        handleDelete={handleDeleteRight}
        sendDataToParentForEdit={handleEditRight}
        sendDataToParentForAdd={handleAddRight}
         />
    </Grid>
</Grid>
    
    </div>
    
  );
}
