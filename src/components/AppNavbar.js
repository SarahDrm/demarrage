/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday September 3rd 2020
 */

import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import {
    Nav,
    Navbar,
    NavItem,
    Collapse,
    DropdownMenu,
    NavbarToggler,
    DropdownToggle,
    UncontrolledDropdown
} from 'reactstrap'

import logo from '../logoPanda.png'
import { t } from '../services/i18n'
import { locales } from '../config/i18n'
import { getLocaleFromPath, prefixPath } from '../services/i18n/util'
import Logout from '../containers/Logout'

class AppNavbar extends Component {
    constructor(props) {
        super(props)
        this.state = { isOpen: false }
    }

    toggle() {
        this.setState(prevState => ({ isOpen: !prevState.isOpen }))
    }

    refreshPage() {
        window.location.reload(false);
      }

    render() { 

        return (
            <Navbar fixed="top" color="light" light expand="md">

                <Link className="navbar-brand" to={prefixPath("/MenuClient", getLocaleFromPath(this.props.location.pathname))} >
                    <img
                    src={logo}
                    width="40"
                    height="30"
                    className="d-inline-block align-top"
                    alt={t('app_name')}
                    />
                {t('app_name')} 
                </Link>
            

                <NavbarToggler onClick={() => this.toggle()} /> 

                <Collapse isOpen={this.state.isOpen} navbar>
                    <span className="navbar-text small d-inline-block pr-4">
                        —  protecting people
                    </span>

                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <Link to={prefixPath(`/InternationalElements`, getLocaleFromPath(this.props.location.pathname))}> 
                                 &nbsp; {t('internationalisation')} 
                            </Link>
                        </NavItem>
                       <NavItem>
                              <Link to={prefixPath(`/MenuClient`, getLocaleFromPath(this.props.location.pathname))}> 
                              &nbsp;  MenuClient
                            </Link>
                       </NavItem> 
                       
                    </Nav>
                   


                    <Nav className="ml-auto" navbar>
                       
                         <UncontrolledDropdown nav inNavbar>  
                            <DropdownToggle nav caret>
                                <span
                                    role="img"
                                    aria-label="globe"
                                    className="globe-icon" 
                                >
                                    🌐
                                </span>
                            
                                {t('language')} 
                            </DropdownToggle>

                            <DropdownMenu right>
                                {locales.map(locale => (                                   
                                   <Link
                                   key={locale.code}
                                   to={`/${locale.code}`}
                                   className="dropdown-item"
                                    >
                                   {locale.name}
                                </Link>
                                )                                
                                )}
                            </DropdownMenu>
                            
                        </UncontrolledDropdown>
                        &nbsp;  &nbsp;
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <Logout></Logout>
                            </NavItem>
                        </Nav>


                    </Nav>
                </Collapse> 
                
            </Navbar>
        )
    }
}


export default withRouter( AppNavbar )
